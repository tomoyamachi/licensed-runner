# licensed-runner

ライブラリのLicenseチェックをGitLab Runnerで実行する

## [github/licensed](https://github.com/github/licensed)

`licensed status` で許可されていないパッケージがある場合はexit code 1で終了する。
ライセンス情報を`.licenses/<package manager>`以下に保存している。
公式のコンテナイメージがないため、CI環境で必要なパッケージマネージャ(pipenv,go modなど)を自力でインストールする必要がある。

### 許可を追加

`.licensed.yml` を編集する。

### リスト表示

```
$ licensed list
```

## [github.com/pivotal/LicenseFinder](https://github.com/pivotal/LicenseFinder)

```
$ license_finder
# 許可されていないパッケージが並ぶ. package名, version, license
Dependencies that need approval:
certifi, 2022.5.18, "Mozilla Public License 2.0"
click, 7.0, BSD
distlib, 0.3.4, "Python Software Foundation License"
filelock, 3.7.0, "Public Domain"
itsdangerous, 1.1.0, BSD
jinja2, 2.10, BSD
markupsafe, 1.1.0, BSD
tornado, 5.1, "Apache 2.0"
werkzeug, 0.14.1, BSD
```

許可されていないパッケージがある場合、exit codeが1で終了し、CIが失敗する。

### 許可を追加

`doc/dependency_decisions.yml` を編集する。以下のコマンドで、許可するライセンス/パッケージを追加することも可能。

- 特定のライセンスを許可
```shell
$ cd /path/to/project
$ docker run --rm -v $(pwd):/app -it licensefinder/license_finder bash -l -c "cd /app && license_finder permitted_licenses add MIT --who <username> --why '<reason>'"
```

- 特定のパッケージを許可

```shell
$ cd /path/to/project
$ docker run --rm -v $(pwd):/app -it licensefinder/license_finder bash -l -c "cd /app && license_finder approvals add <package1> <package2>  --who tomoyamachi --why '<reason>'"
```

- 特定のパッケージ/バージョンを許可

```shell
$ cd /path/to/project
$ docker run --rm -v $(pwd):/app -it licensefinder/license_finder bash -l -c "cd /app && license_finder approvals add <package1> --version <version> --who tomoyamachi --why '<reason>'"
```

### リスト表示

```shell
$ docker run --rm -v $(pwd):/app -it licensefinder/license_finder bash -l -c "cd /app && license_finder report'

# htmlで表示するとサマリや承認理由などもレポートとして書き出し可能
$ docker run --rm -v $(pwd):/app -it licensefinder/license_finder bash -l -c "cd /app && license_finder report --format html'
```